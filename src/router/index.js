import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
    meta: { layout: "home" },
  },
  {
    path: "/courses",
    name: "Сourses",
    component: () => import("../views/Сourses.vue"),
    meta: { layout: "course" },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
